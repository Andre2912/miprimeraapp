package com.example.miprimeraapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editContraseña,editUsuario;
    private Button IniciarSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        IniciarSesion = findViewById(R.id.buttonIniciarSesion);
        IniciarSesion.setOnClickListener(this);
        editContraseña = findViewById(R.id.editContraseña);
        editUsuario = findViewById(R.id.editUsuario)  ;
    }

    private String usuario = "jorge";
    private String contraseña = "1234";


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.buttonIniciarSesion) {
            if (usuario.equals(editUsuario.getText().toString()) && contraseña.equals(editContraseña.getText().toString())) {
                Intent i = new Intent(this,Home.class);
                startActivity(i);
            }
        }
    }
}