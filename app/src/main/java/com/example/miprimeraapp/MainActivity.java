package com.example.miprimeraapp;

        import android.content.Intent;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    //1. boton del layout debe ser atributo en la clase
    Button botonRegistrarse,botonInicioSesion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //2. Asociar el atributo con el view del layout usando su ID
        botonRegistrarse = findViewById(R.id.buttonRegistrarse);
        //3. Habilitar el boton para que sea clickeable
        botonRegistrarse.setOnClickListener(this);
        botonInicioSesion = findViewById(R.id.buttonInicioSesion);
        botonInicioSesion.setOnClickListener(this);
    }

    //5. Implementar el metodo onclick
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.buttonRegistrarse) {
            Intent i = new Intent(this, SignUpActivity.class);
            startActivity(i);
        }
        if (view.getId() == R.id.buttonInicioSesion) {
            Intent i = new Intent(this,LoginActivity.class);
            startActivity(i);
        }

    }
}